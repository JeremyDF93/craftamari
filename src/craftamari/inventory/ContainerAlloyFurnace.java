package craftamari.inventory;

import craftamari.inventory.slot.SlotAlloyFurnace;
import craftamari.tile.TileEntityAlloyFurnace;
import net.minecraft.src.Container;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.ICrafting;
import net.minecraft.src.InventoryPlayer;
import net.minecraft.src.ItemStack;
import net.minecraft.src.AlloyFurnaceRecipes;
import net.minecraft.src.Slot;
import net.minecraft.src.TileEntityFurnace;

public class ContainerAlloyFurnace extends Container {
    private TileEntityAlloyFurnace tileEntity;
    
    private int lastCookTime = 0;
    private int lastBurnTime = 0;
    private int lastItemBurnTime = 0;
    
    public ContainerAlloyFurnace(InventoryPlayer inventory, TileEntityAlloyFurnace tileEntity) {
        this.tileEntity = tileEntity;
        
        addSlotToContainer(new Slot(tileEntity, 0, 46, 17));
        addSlotToContainer(new Slot(tileEntity, 1, 66, 17));
        addSlotToContainer(new Slot(tileEntity, 2, 56, 53));
        addSlotToContainer(new SlotAlloyFurnace(inventory.player, tileEntity, 3, 116, 35));
        
        for (int y = 0; y < 3; y++) {
            for (int x = 0; x < 9; x++) {
                addSlotToContainer(new Slot(inventory, x + y * 9 + 9, 8 + x * 18, 84 + y * 18));
            }
        }
        
        for (int x = 0; x < 9; x++) {
            addSlotToContainer(new Slot(inventory, x, 8 + x * 18, 142));
        }
    }
    
    @Override
    public void addCraftingToCrafters(ICrafting crafting) {
        super.addCraftingToCrafters(crafting);
        crafting.sendProgressBarUpdate(this, 0, tileEntity.furnaceCookTime);
        crafting.sendProgressBarUpdate(this, 1, tileEntity.furnaceBurnTime);
        crafting.sendProgressBarUpdate(this, 2, tileEntity.itemBurnTime);
    }

    @Override
    public void detectAndSendChanges() {
        super.detectAndSendChanges();

        for (int i = 0; i < crafters.size(); ++i) {
            ICrafting crafting = (ICrafting) this.crafters.get(i);

            if (lastCookTime != tileEntity.furnaceCookTime) {
                crafting.sendProgressBarUpdate(this, 0, tileEntity.furnaceCookTime);
            }

            if (lastBurnTime != tileEntity.furnaceBurnTime) {
                crafting.sendProgressBarUpdate(this, 1, tileEntity.furnaceBurnTime);
            }

            if (lastItemBurnTime != tileEntity.itemBurnTime) {
                crafting.sendProgressBarUpdate(this, 2, tileEntity.itemBurnTime);
            }
        }

        lastCookTime = tileEntity.furnaceCookTime;
        lastBurnTime = tileEntity.furnaceBurnTime;
        lastItemBurnTime = tileEntity.itemBurnTime;
    }
    
    @Override
    public void updateProgressBar(int bar, int value) {
        if (bar == 0) {
            tileEntity.furnaceCookTime = value;
        }

        if (bar == 1) {
            tileEntity.furnaceBurnTime = value;
        }

        if (bar == 2) {
            tileEntity.itemBurnTime = value;
        }
    }
    
    @Override
    public ItemStack transferStackInSlot(EntityPlayer player, int slot) {
        ItemStack itemStack1 = null;
        Slot inventorySlot = (Slot) inventorySlots.get(slot);
        
        if (inventorySlot != null && inventorySlot.getHasStack()) {
            ItemStack itemStack2 = inventorySlot.getStack();
            itemStack1 = itemStack2.copy();
            
            if (slot == 3) {
                if (!mergeItemStack(itemStack2, 4, 40, true)) {
                    return null;
                }
                
                inventorySlot.onSlotChange(itemStack2, itemStack1);
            } else if (slot >= 4 && slot < 40) {
                if (AlloyFurnaceRecipes.canSmelt(itemStack2)) {
                    if (!mergeItemStack(itemStack2, 0, 2, false)) {
                        return null;
                    }
                } else if (TileEntityFurnace.isItemFuel(itemStack2)) {
                    if (!mergeItemStack(itemStack2, 2, 3, false)) {
                        return null;
                    }
                } else if (slot >= 4 && slot < 31) {
                    if (!mergeItemStack(itemStack2, 31, 40, false)) {
                        return null;
                    }
                } else if (slot >= 31 && slot < 40) {
                    if (!mergeItemStack(itemStack2, 4, 31, false)) {
                        return null;
                    }
                }
            } else if (!mergeItemStack(itemStack2, 4, 40, false)) {
                return null;
            }
            
            if (itemStack2.stackSize == 0) {
                inventorySlot.putStack(null);
            } else {
                inventorySlot.onSlotChanged();
            }
            
            if (itemStack2.stackSize == itemStack1.stackSize) {
                return null;
            }
            
            inventorySlot.onPickupFromSlot(player, itemStack2);
        }
        
        return itemStack1;
    }

    @Override
    public boolean canInteractWith(EntityPlayer player) {
        return tileEntity.isUseableByPlayer(player);
    }

}
