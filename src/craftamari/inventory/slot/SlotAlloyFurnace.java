package craftamari.inventory.slot;

import craftamari.tile.TileEntityAlloyFurnace;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.EntityXPOrb;
import net.minecraft.src.ItemStack;
import net.minecraft.src.MathHelper;
import net.minecraft.src.AlloyFurnaceRecipes;
import net.minecraft.src.Slot;

public class SlotAlloyFurnace extends Slot {
    private EntityPlayer player;
    private int craftCount;
    
    public SlotAlloyFurnace(EntityPlayer player, TileEntityAlloyFurnace tileEntity, int slot, int x, int y) {
        super(tileEntity, slot, x, y);
        this.player = player;
    }
    
    @Override
    public ItemStack decrStackSize(int amount) {
        if (getHasStack()) {
            craftCount += Math.min(amount, this.getStack().stackSize);
        }
        
        return super.decrStackSize(amount);
    }
    
    @Override
    public boolean isItemValid(ItemStack itemStack) {
        return false;
    }
    
    @Override
    public void onPickupFromSlot(EntityPlayer player, ItemStack itemStack) {
        onCrafting(itemStack);
        super.onPickupFromSlot(player, itemStack);
    }
    
    @Override
    protected void onCrafting(ItemStack itemStack) {
        itemStack.onCrafting(player.worldObj, player, craftCount);
        
        if (!player.worldObj.isRemote) {
            int i;
            int craftCount = this.craftCount;
            float experience = AlloyFurnaceRecipes.getExperience(itemStack);
            
            if (experience == 0.0F) {
                craftCount = 0;
            } else if (experience < 1.0F) {
                i = MathHelper.floor_float(craftCount * experience);
                
                if (i < MathHelper.ceiling_float_int(craftCount * experience) && (float) Math.random() < craftCount * experience -  i) {
                    i++;
                }
                
                craftCount = i;
            }
            
            while (craftCount > 0) {
                i = EntityXPOrb.getXPSplit(craftCount);
                craftCount -= i;
                player.worldObj.spawnEntityInWorld(new EntityXPOrb(player.worldObj, player.posX + 0.5D, player.posY + 0.5D, player.posZ + 0.5D, i));
            }
        }
        
        craftCount = 0;
    }
    
    @Override
    protected void onCrafting(ItemStack itemStack, int amount) {
        craftCount += amount;
        onCrafting(itemStack);
    }
}
