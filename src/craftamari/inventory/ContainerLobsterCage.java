package craftamari.inventory;

import craftamari.tile.TileEntityLobsterCage;
import net.minecraft.src.Container;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.InventoryPlayer;
import net.minecraft.src.ItemStack;
import net.minecraft.src.Slot;

public class ContainerLobsterCage extends Container {
    private TileEntityLobsterCage tileEntity;
    
    public ContainerLobsterCage(InventoryPlayer inventory, TileEntityLobsterCage tileEntity) {
        this.tileEntity = tileEntity;
        
        for (int y = 0; y < 3; y++) {
            for (int x = 0; x < 3; x++) {
                addSlotToContainer(new Slot(tileEntity, x + y * 3, 62 + x * 18, 17 + y * 18));
            }
        }
        
        for (int y = 0; y < 3; y++) {
            for (int x = 0; x < 9; x++) {
                addSlotToContainer(new Slot(inventory, x + y * 9 + 9, 8 + x * 18, 84 + y * 18));
            }
        }
        
        for (int x = 0; x < 9; x++) {
            addSlotToContainer(new Slot(inventory, x, 8 + x * 18, 142));
        }
    }
    
    @Override
    public ItemStack transferStackInSlot(EntityPlayer player, int slot) {
        ItemStack itemStack1 = null;
        Slot inventorySlot = (Slot) inventorySlots.get(slot);

        if (inventorySlot != null && inventorySlot.getHasStack()) {
            ItemStack itemStack2 = inventorySlot.getStack();
            itemStack1 = itemStack2.copy();

            if (slot < 9) {
                if (!mergeItemStack(itemStack2, 9, 45, true)) {
                    return null;
                }
            } else if (slot >= 9 && slot < 36) {
                if (!mergeItemStack(itemStack2, 36, 45, false)) {
                    return null;
                }
            } else if (slot >= 36 && slot < 45) {
                if (!mergeItemStack(itemStack2, 9, 36, false)) {
                    return null;
                }
            }

            if (itemStack2.stackSize == 0) {
                inventorySlot.putStack((ItemStack) null);
            } else {
                inventorySlot.onSlotChanged();
            }

            if (itemStack2.stackSize == itemStack1.stackSize) {
                return null;
            }

            inventorySlot.onPickupFromSlot(player, itemStack2);
        }

        return itemStack1;
    }

    @Override
    public boolean canInteractWith(EntityPlayer player) {
        return tileEntity.isUseableByPlayer(player);
    }

}
