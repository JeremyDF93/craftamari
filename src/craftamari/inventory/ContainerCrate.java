package craftamari.inventory;

import craftamari.tile.TileEntityCrate;
import net.minecraft.src.Container;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.InventoryPlayer;
import net.minecraft.src.ItemStack;
import net.minecraft.src.Slot;

public class ContainerCrate extends Container {
    private TileEntityCrate tileEntity;
    
    public ContainerCrate(InventoryPlayer inventory, TileEntityCrate tileEntity) {
        this.tileEntity = tileEntity;
        
        addSlotToContainer(new Slot(tileEntity, 0, 80, 35));
        
        for (int y = 0; y < 3; y++) {
            for (int x = 0; x < 9; x++) {
                addSlotToContainer(new Slot(inventory, x + y * 9 + 9, 8 + x * 18, 84 + y * 18));
            }
        }
        
        for (int x = 0; x < 9; x++) {
            addSlotToContainer(new Slot(inventory, x, 8 + x * 18, 142));
        }
    }
    
    @Override
    public ItemStack transferStackInSlot(EntityPlayer player, int slot) {
        ItemStack itemStack1 = null;
        Slot inventorySlot = (Slot) inventorySlots.get(slot);
        
        if (inventorySlot != null && inventorySlot.getHasStack()) {
            ItemStack itemStack2 = inventorySlot.getStack();
            itemStack1 = itemStack2.copy();
            
            if (slot == 0) {
                if (!mergeItemStack(itemStack2, 1, 37, true)) {
                    return null;
                }
                
                inventorySlot.onSlotChange(itemStack2, itemStack1);
            } else if (slot >= 1 && slot < 37) {
                if (!mergeItemStack(itemStack2, 0, 1, false)) {
                    if (slot >= 1 && slot < 28) {
                        if (!mergeItemStack(itemStack2, 28, 37, false)) {
                            return null;
                        }
                    } else if (slot >= 28 && slot < 37) {
                        if (!mergeItemStack(itemStack2, 1, 28, false)) {
                            return null;
                        }
                    }
                }
            } else if (!mergeItemStack(itemStack2, 1, 37, false)) {
                return null;
            }
            
            if (itemStack2.stackSize == 0) {
                inventorySlot.putStack(null);
            } else {
                inventorySlot.onSlotChanged();
            }
            
            if (itemStack2.stackSize == itemStack1.stackSize) {
                return null;
            }
            
            inventorySlot.onPickupFromSlot(player, itemStack2);
        }
        
        return itemStack1;
    }

    @Override
    public boolean canInteractWith(EntityPlayer player) {
        return tileEntity.isUseableByPlayer(player);
    }

}
