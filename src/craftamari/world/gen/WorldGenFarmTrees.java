package craftamari.world.gen;

import java.util.ArrayList;
import java.util.Random;

import net.minecraft.src.Block;
import net.minecraft.src.Direction;
import net.minecraft.src.World;
import net.minecraft.src.WorldGenerator;

public class WorldGenFarmTrees extends WorldGenerator {

    private final int minTreeHeight;

    private final boolean vinesGrow;

    private final int metaWood;

    private final int metaLeaves;
    
    private final int heightVariance;
    
    public ArrayList<int[]> leaves = new ArrayList<int[]>();

    public WorldGenFarmTrees(boolean par1) {
        this(par1, 3, 0, 0, 1, false);
    }

    public WorldGenFarmTrees(boolean par1, int par2, int par3, int par4, int var, boolean par5) {
        super(par1);
        this.minTreeHeight = par2;
        this.metaWood = par3;
        this.metaLeaves = par4;
        this.vinesGrow = par5;
        this.heightVariance = var;
    }
    
    public boolean placeBlock(World w, int x, int y, int z, boolean leaf) {
    	int b = w.getBlockId(x, y, z);
    	if(b==0 || b==Block.leaves.blockID || b==Block.leavesFruit.blockID || b==Block.woodFruit.blockID) {
    		int[] temp={x,y,z};
    		if(leaf) {
    			leaves.add(temp);
    		}
    		return true;
    	}
    	return false;
    }

    public boolean generate(World par1World, Random par2Random, int par3, int par4, int par5) {
        int var6 = par2Random.nextInt(heightVariance) + this.minTreeHeight;
        boolean var7 = true;

        if (par4 >= 1 && par4 + var6 + 1 <= 256) {
            int var8;
            byte var9;
            int var11;
            int var12;
            
            if(!placeBlock(par1World, par3, par4, par5, false) || !placeBlock(par1World, par3, par4+1, par5, false) || !placeBlock(par1World, par3, par4+1, par5, false) || !placeBlock(par1World, par3, par4+1, par5, false)) {
            	var7=false;
            }

            if (!var7) {
                return false;
            } else {
                var8 = par1World.getBlockId(par3, par4 - 1, par5);

                if (var8 == Block.tilledField.blockID && par4 < 256 - var6 - 1) {
                    this.setBlock(par1World, par3, par4 - 1, par5, Block.dirt.blockID);
                    var9 = 3;
                    byte var19 = 0;
                    int var13;
                    int var14;
                    int var15;
                    
                    placeBlock(par1World, par3, par4 + 4, par5, true);
                    placeBlock(par1World, par3 + 1, par4 + 3, par5, true);
                    placeBlock(par1World, par3 - 1, par4 + 3, par5, true);
                    placeBlock(par1World, par3, par4 + 3, par5 + 1, true);
                    placeBlock(par1World, par3, par4 + 3, par5 - 1, true);
                    placeBlock(par1World, par3 + 1, par4 + 2, par5, true);
                    placeBlock(par1World, par3 - 1, par4 + 2, par5, true);
                    placeBlock(par1World, par3, par4 + 2, par5 + 1, true);
                    placeBlock(par1World, par3, par4 + 2, par5 - 1, true);
                    placeBlock(par1World, par3 + 1, par4 + 2, par5 -1, true);
                    placeBlock(par1World, par3 - 1, par4 + 2, par5 + 1, true);
                    placeBlock(par1World, par3 + 1, par4 + 2, par5 + 1, true);
                    placeBlock(par1World, par3 - 1, par4 + 2, par5 - 1, true);
                    placeBlock(par1World, par3 + 2, par4 + 2, par5, true);
                    placeBlock(par1World, par3 - 2, par4 + 2, par5, true);
                    placeBlock(par1World, par3, par4 + 2, par5 + 2, true);
                    placeBlock(par1World, par3, par4 + 2, par5 - 2, true);
                    
                    if(this.leaves.size()==17) {
	                    this.setBlock(par1World, par3, par4 , par5, Block.woodFruit.blockID);
	                    this.setBlock(par1World, par3, par4 + 1, par5, Block.woodFruit.blockID);
	                    this.setBlock(par1World, par3, par4 + 2, par5, Block.woodFruit.blockID);
	                    this.setBlock(par1World, par3, par4 + 3, par5, Block.woodFruit.blockID);
	                    
	                    for(int[] b : leaves) {
	                    	if(par1World.getBlockId(b[0], b[1], b[2])!=Block.woodFruit.blockID) {
	                    		this.setBlockAndMetadata(par1World, b[0], b[1], b[2], Block.leavesFruit.blockID, this.metaLeaves);
	                    	}
	                    }
                    } else {
                    	return false;
                    }

                    return true;
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
    }
}
