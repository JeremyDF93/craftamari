package craftamari.item;

import net.minecraft.src.CreativeTabs;
import net.minecraft.src.EnumArmorMaterial;
import net.minecraft.src.EnumCapeMaterial;
import net.minecraft.src.Icon;
import net.minecraft.src.IconRegister;
import net.minecraft.src.Item;
import net.minecraft.src.ItemStack;
import net.minecraft.src.NBTTagCompound;

public class ItemCape extends Item {
    private Icon iconBackground;
    private EnumCapeMaterial material;
    
    public int damageReduceAmount;
    
    public ItemCape(int id, EnumCapeMaterial material) {
        super(id);
        this.material = material;
        this.damageReduceAmount = material.getDamageReductionAmount();
        setMaxStackSize(1);
        setCreativeTab(CreativeTabs.tabCombat);
    }
    
    @Override
    public void registerIcons(IconRegister iconRegister) {
        super.registerIcons(iconRegister);
        iconBackground = iconRegister.registerIcon("slot_empty_cape");
    }
    
    public static Icon getIconBackground() {
        return Item.capeObsidian.iconBackground;
    }

    public String getCapeTexture() {
        return "/textures/capes/" + this.unlocalizedName + ".png";
    }
    
    public EnumCapeMaterial getCapeMaterial() {
        return material;
    }
    
    /**
     * Return whether the specified armor ItemStack has a color.
     */
    public boolean hasColor(ItemStack par1ItemStack) {
        return this.material != EnumCapeMaterial.CLOTH ? false : (!par1ItemStack.hasTagCompound() ? false : (!par1ItemStack.getTagCompound().hasKey("display") ? false : par1ItemStack.getTagCompound().getCompoundTag("display").hasKey("color")));
    }
    
    /**
     * Return the color for the specified cape ItemStack.
     */
    public int getColor(ItemStack itemStack) {
        if (this.material != EnumCapeMaterial.CLOTH) {
            return -1;
        } else {
            NBTTagCompound var2 = itemStack.getTagCompound();

            if (var2 == null) {
                return 10511680;
            } else {
                NBTTagCompound var3 = var2.getCompoundTag("display");
                return var3 == null ? 10511680 : (var3.hasKey("color") ? var3.getInteger("color") : 10511680);
            }
        }
    }
    
    public int getColorFromItemStack(ItemStack itemStack, int i) {
        if (i > 0) {
            return 16777215;
        } else {
            int color = this.getColor(itemStack);

            if (color < 0) {
                color = 16777215;
            }

            return color;
        }
    }
    
    public void setColor(ItemStack itemStack, int color) {
        if (this.material != EnumCapeMaterial.CLOTH) {
            throw new UnsupportedOperationException("Can\'t dye non-cloth!");
        } else {
            NBTTagCompound compoundTag = itemStack.getTagCompound();

            if (compoundTag == null) {
                compoundTag = new NBTTagCompound();
                itemStack.setTagCompound(compoundTag);
            }

            NBTTagCompound var4 = compoundTag.getCompoundTag("display");

            if (!compoundTag.hasKey("display")) {
                compoundTag.setCompoundTag("display", var4);
            }

            var4.setInteger("color", color);
        }
    }
}
