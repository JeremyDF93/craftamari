package craftamari.item;

import net.minecraft.src.Block;
import net.minecraft.src.ColorizerFoliage;
import net.minecraft.src.Icon;
import net.minecraft.src.ItemBlock;
import net.minecraft.src.ItemStack;
import craftamari.block.BlockLeavesFruit;

public class ItemLeavesFruit extends ItemBlock {
    public ItemLeavesFruit(int par1) {
        super(par1);
        this.setMaxDamage(0);
        this.setHasSubtypes(true);
    }

    /**
     * Returns the metadata of the block which this Item (ItemBlock) can place
     */
    public int getMetadata(int par1) {
        return par1 | 1;
    }

    /**
     * Gets an icon index based on an item's damage value
     */
    public Icon getIconFromDamage(int par1) {
        return Block.leavesFruit.getIcon(0, par1);
    }

    public int getColorFromItemStack(ItemStack par1ItemStack, int par2) {
        int var3 = par1ItemStack.getItemDamage();
        return ColorizerFoliage.getFoliageColorOrange();
    }

    /**
     * Returns the unlocalized name of this item. This version accepts an ItemStack so different stacks can have
     * different names based on their damage or NBT.
     */
    public String getUnlocalizedName(ItemStack par1ItemStack) {
        int var2 = par1ItemStack.getItemDamage();

        if (var2 < 0 || var2 >= BlockLeavesFruit.LEAF_TYPES.length) {
            var2 = 0;
        }

        return super.getUnlocalizedName() + "." + BlockLeavesFruit.LEAF_TYPES[var2];
    }
}
