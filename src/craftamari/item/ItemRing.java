package craftamari.item;

import java.util.List;

import net.minecraft.src.CreativeTabs;
import net.minecraft.src.Icon;
import net.minecraft.src.IconRegister;
import net.minecraft.src.Item;
import net.minecraft.src.ItemStack;

public class ItemRing extends Item {
    public static String[] ringTypes = { "gold", "sapphire", "emerald", "ruby", "diamond", "dragonstone", "onyx" };
    
    private Icon iconStone;
    private Icon iconBackground;
    
    public ItemRing(int id) {
        super(id);
        setMaxStackSize(1);
        setHasSubtypes(true);
        setCreativeTab(CreativeTabs.tabMisc);
    }
    
    @Override
    public boolean requiresMultipleRenderPasses() {
        return true;
    }
    
    @Override
    public int getColorFromItemStack(ItemStack itemStack, int pass) {
        return pass > 0 ? getColorFromDamage(itemStack.getItemDamage()) : 0xffffff;
    }
    
    @Override
    public Icon getIconFromDamageForRenderPass(int metadata, int pass) {
        return pass > 0 ? iconStone : super.getIconFromDamageForRenderPass(metadata, pass);
    }
    
    public int getColorFromDamage(int metadata) {
        if (metadata == 0) {
            return 0xda9100;
        } else if (metadata == 1) {
            return 0x0f52ba;
        } else if (metadata == 2) {
            return 0x00a550;
        } else if (metadata == 3) {
            return 0xe0115f;
        } else if (metadata == 4) {
            return 0xffffff;
        } else if (metadata == 5) {
            return 0x800080;
        } else if (metadata == 6) {
            return 0x353839;
        } else {
            return 0xffffff;
        }
    }
    
    @Override
    public void registerIcons(IconRegister iconRegister) {
        itemIcon = iconRegister.registerIcon("ring");
        iconStone = iconRegister.registerIcon("ring_contents");
        iconBackground = iconRegister.registerIcon("slot_empty_ring");
    }
    
    @Override
    public String getUnlocalizedName(ItemStack itemStack) {
        return super.getUnlocalizedName(itemStack) + "." + ringTypes[itemStack.getItemDamage()];
    }
    
    @Override
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public void getSubItems(int id, CreativeTabs creativeTabs, List list) {
        for (int i = 0; i < ringTypes.length; i++) {
            list.add(new ItemStack(id, 1, i));
        }
    }
    
    public static Icon getIconBackground() {
        return Item.ring.iconBackground;
    }
}
