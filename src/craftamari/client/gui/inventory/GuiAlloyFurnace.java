package craftamari.client.gui.inventory;

import net.minecraft.src.GuiContainer;
import net.minecraft.src.InventoryPlayer;
import net.minecraft.src.StatCollector;

import org.lwjgl.opengl.GL11;

import craftamari.inventory.ContainerAlloyFurnace;
import craftamari.tile.TileEntityAlloyFurnace;


public class GuiAlloyFurnace extends GuiContainer {
    private TileEntityAlloyFurnace tileEntity;

    public GuiAlloyFurnace(InventoryPlayer inventory, TileEntityAlloyFurnace tileEntity) {
        super(new ContainerAlloyFurnace(inventory, tileEntity));
        this.tileEntity = tileEntity;
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int x, int y) {
        fontRenderer.drawString(StatCollector.translateToLocal("container.alloyFurnace"), 54, 6, 4210752);
        fontRenderer.drawString(StatCollector.translateToLocal("container.inventory"), 8, ySize - 96 + 2, 4210752);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float var1, int x, int y) {
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        mc.renderEngine.bindTexture("/gui/alloy_furnace.png");
        int pointX = (width - xSize) / 2;
        int pointY = (height - ySize) / 2;
        drawTexturedModalRect(pointX, pointY, 0, 0, xSize, ySize);
        int i;
        
        if (tileEntity.isBurning()) {
            i = tileEntity.getBurnTimeRemainingScaled(12);
            this.drawTexturedModalRect(pointX + 56, pointY + 36 + 12 - i, 176, 12 - i, 14, i + 2);
        }

        i = tileEntity.getCookProgressScaled(24);
        this.drawTexturedModalRect(pointX + 79, pointY + 34, 176, 14, i + 1, 16);
    }
}
