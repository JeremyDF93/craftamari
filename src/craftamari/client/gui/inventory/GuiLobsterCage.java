package craftamari.client.gui.inventory;

import net.minecraft.src.GuiContainer;
import net.minecraft.src.InventoryPlayer;
import net.minecraft.src.StatCollector;

import org.lwjgl.opengl.GL11;

import craftamari.inventory.ContainerLobsterCage;
import craftamari.tile.TileEntityLobsterCage;


public class GuiLobsterCage extends GuiContainer {
    public GuiLobsterCage(InventoryPlayer inventory, TileEntityLobsterCage tileEntity) {
        super(new ContainerLobsterCage(inventory, tileEntity));
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int x, int y) {
        fontRenderer.drawString(StatCollector.translateToLocal("container.cage"), 54, 6, 4210752);
        fontRenderer.drawString(StatCollector.translateToLocal("container.inventory"), 8, ySize - 96 + 2, 4210752);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float var1, int x, int y) {
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        mc.renderEngine.bindTexture("/gui/cage.png");
        int pointX = (width - xSize) / 2;
        int pointY = (height - ySize) / 2;
        drawTexturedModalRect(pointX, pointY, 0, 0, xSize, ySize);
    }
}
