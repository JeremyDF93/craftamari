package craftamari.entity;

import net.minecraft.src.Entity;
import net.minecraft.src.EntitySpider;
import net.minecraft.src.MathHelper;
import net.minecraft.src.World;

public class EntityTinySpider extends EntitySpider {
    public EntityTinySpider(World par1World) {
        super(par1World);
        this.texture = "/mob/spider.png";
        this.setSize(1.4F, 0.9F);
        this.moveSpeed = 0.6F;
        this.setScale(0.28F);
        this.experienceValue = 1;
    }

    public int getMaxHealth() {
        return 4;
    }
    
    protected float getSoundPitch() {
        return (this.rand.nextFloat() - this.rand.nextFloat()) * 0.2F + 1.4F;
    }
    
    protected float getSoundVolume() {
        return 0.6F;
    }

    public double getMountedYOffset() {
        return (double)this.height * 0.75D - (this.getScale() * 0.5D);
    }

    protected Entity findPlayerToAttack() {
        float var1 = this.getBrightness(1.0F);

        if (var1 < 0.5F) {
            double var2 = 14.0D;
            return this.worldObj.getClosestVulnerablePlayerToEntity(this, var2);
        } else {
            return null;
        }
    }

    protected void attackEntity(Entity par1Entity, float par2) {
        float var3 = this.getBrightness(1.0F);

        if (var3 > 0.5F && this.rand.nextInt(100) == 0) {
            this.entityToAttack = null;
        } else {
            if (par2 > 2.0F && par2 < 6.0F && this.rand.nextInt(10) == 0) {
                if (this.onGround) {
                    double var4 = par1Entity.posX - this.posX;
                    double var6 = par1Entity.posZ - this.posZ;
                    float var8 = MathHelper.sqrt_double(var4 * var4 + var6 * var6);
                    this.motionX = var4 / (double)var8 * 0.5D * 0.800000011920929D + this.motionX * 0.20000000298023224D;
                    this.motionZ = var6 / (double)var8 * 0.5D * 0.800000011920929D + this.motionZ * 0.20000000298023224D;
                    this.motionY = 0.4000000059604645D;
                }
            } else {
                super.attackEntity(par1Entity, par2);
            }
        }
    }

    protected void dropFewItems(boolean par1, int par2) {
    	if(this.rand.nextBoolean()) {
    		super.dropFewItems(false, par2);
    	}
    }

    public void initCreature() {

    }
}
