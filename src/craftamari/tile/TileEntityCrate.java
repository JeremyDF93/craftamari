package craftamari.tile;

import net.minecraft.src.EntityPlayer;
import net.minecraft.src.IInventory;
import net.minecraft.src.ItemStack;
import net.minecraft.src.NBTTagCompound;
import net.minecraft.src.NBTTagList;
import net.minecraft.src.TileEntity;

public class TileEntityCrate extends TileEntity implements IInventory {
    private ItemStack[] crateItemStacks = new ItemStack[1];
    
    @Override
    public int getSizeInventory() {
        return crateItemStacks.length;
    }

    @Override
    public ItemStack getStackInSlot(int slot) {
        return crateItemStacks[slot];
    }

    @Override
    public ItemStack decrStackSize(int slot, int amount) {
        if (crateItemStacks[slot] != null) {
            ItemStack itemStack;
            
            if (crateItemStacks[slot].stackSize <= amount) {
                itemStack = crateItemStacks[slot];
                crateItemStacks[slot] = null;
                return itemStack;
            } else {
                itemStack = crateItemStacks[slot].splitStack(amount);
                
                if (crateItemStacks[slot].stackSize == 0) {
                    crateItemStacks[slot] = null;
                }
                
                return itemStack;
            }
        }
        
        return null;
    }

    @Override
    public ItemStack getStackInSlotOnClosing(int slot) {
        if (crateItemStacks[slot] != null) {
            ItemStack itemStack = crateItemStacks[slot];
            crateItemStacks[slot] = null;
            return itemStack;
        }
        
        return null;
    }

    @Override
    public void setInventorySlotContents(int slot, ItemStack itemStack) {
        crateItemStacks[slot] = itemStack;
        
        if (itemStack != null && itemStack.stackSize > getInventoryStackLimit()) {
            itemStack.stackSize = getInventoryStackLimit();
        }
    }

    @Override
    public String getInvName() {
        return "container.crate";
    }

    @Override
    public int getInventoryStackLimit() {
        return 64;
    }

    @Override
    public boolean isUseableByPlayer(EntityPlayer player) {
        return worldObj.getBlockTileEntity(xCoord, yCoord, zCoord) != this ? false : player.getDistanceSq(xCoord + 0.5D, yCoord + 0.5D, zCoord + 0.5D) <= 64.0D;
    }

    @Override
    public void openChest() {}

    @Override
    public void closeChest() {}
    
    @Override
    public void writeToNBT(NBTTagCompound compoundTag1) {
        super.writeToNBT(compoundTag1);
        NBTTagList listTag = new NBTTagList();
        
        for (int i = 0; i < crateItemStacks.length; i++) {
            if (crateItemStacks[i] != null) {
                NBTTagCompound compoundTag2 = new NBTTagCompound();
                compoundTag2.setByte("Slot", (byte) i);
                crateItemStacks[i].writeToNBT(compoundTag2);
                listTag.appendTag(compoundTag2);
            }
        }
        
        compoundTag1.setTag("Items", listTag);
    }
    
    @Override
    public void readFromNBT(NBTTagCompound compoundTag1) {
        super.readFromNBT(compoundTag1);
        NBTTagList listTag = compoundTag1.getTagList("Items");
        crateItemStacks = new ItemStack[getSizeInventory()];
        
        for (int i = 0; i < listTag.tagCount(); i++) {
            NBTTagCompound compoundTag2 = (NBTTagCompound) listTag.tagAt(i);
            byte slot = compoundTag2.getByte("Slot");
            if (slot >= 0 && slot < crateItemStacks.length) {
                crateItemStacks[slot] = ItemStack.loadItemStackFromNBT(compoundTag2);
            }
        }
    }

    @Override
    public boolean isInvNameLocalized() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isStackValidForSlot(int i, ItemStack itemstack) {
        // TODO Auto-generated method stub
        return false;
    }
}
