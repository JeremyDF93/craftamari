package craftamari.tile;

import craftamari.block.BlockAlloyFurnace;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.IInventory;
import net.minecraft.src.Item;
import net.minecraft.src.ItemStack;
import net.minecraft.src.NBTTagCompound;
import net.minecraft.src.NBTTagList;
import net.minecraft.src.AlloyFurnaceRecipes;
import net.minecraft.src.TileEntity;
import net.minecraft.src.TileEntityFurnace;

public class TileEntityAlloyFurnace extends TileEntity implements IInventory {
    private ItemStack[] furnaceItemStacks = new ItemStack[4];
    
    public int itemBurnTime = 0;
    public int furnaceBurnTime = 0;
    public int furnaceCookTime = 0;
    
    @Override
    public int getSizeInventory() {
        return furnaceItemStacks.length;
    }

    @Override
    public ItemStack getStackInSlot(int slot) {
        return furnaceItemStacks[slot];
    }

    @Override
    public ItemStack decrStackSize(int slot, int amount) {
        if (furnaceItemStacks[slot] != null) {
            ItemStack itemStack;
            
            if (furnaceItemStacks[slot].stackSize <= amount) {
                itemStack = furnaceItemStacks[slot];
                furnaceItemStacks[slot] = null;
                return itemStack;
            } else {
                itemStack = furnaceItemStacks[slot].splitStack(amount);
                
                if (furnaceItemStacks[slot].stackSize == 0) {
                    furnaceItemStacks[slot] = null;
                }
                
                return itemStack;
            }
        }
        
        return null;
    }

    @Override
    public ItemStack getStackInSlotOnClosing(int slot) {
        if (furnaceItemStacks[slot] != null) {
            ItemStack itemStack = furnaceItemStacks[slot];
            furnaceItemStacks[slot] = null;
            return itemStack;
        }
        
        return null;
    }

    @Override
    public void setInventorySlotContents(int slot, ItemStack itemStack) {
        furnaceItemStacks[slot] = itemStack;
        
        if (itemStack != null && itemStack.stackSize > getInventoryStackLimit()) {
            itemStack.stackSize = getInventoryStackLimit();
        }
    }

    @Override
    public String getInvName() {
        return "container.alloyFurnace";
    }

    @Override
    public int getInventoryStackLimit() {
        return 64;
    }

    @Override
    public boolean isUseableByPlayer(EntityPlayer player) {
        return worldObj.getBlockTileEntity(xCoord, yCoord, zCoord) != this ? false : player.getDistanceSq(xCoord + 0.5D, yCoord + 0.5D, zCoord + 0.5D) <= 64.0D;
    }

    @Override
    public void openChest() {}

    @Override
    public void closeChest() {}
    
    
    @Override
    public void updateEntity() {
        boolean burning = furnaceBurnTime > 0;
        boolean flag = false;

        if (furnaceBurnTime > 0) {
            --furnaceBurnTime;
        }

        if (!worldObj.isRemote) {
            if (furnaceBurnTime == 0 && canSmelt()) {
                itemBurnTime = furnaceBurnTime = TileEntityFurnace.getItemBurnTime(furnaceItemStacks[2]);

                if (furnaceBurnTime > 0) {
                    flag = true;

                    if (furnaceItemStacks[2] != null) {
                        --furnaceItemStacks[2].stackSize;

                        if (furnaceItemStacks[2].stackSize == 0) {
                            Item item = furnaceItemStacks[2].getItem().getContainerItem();
                            furnaceItemStacks[1] = item != null ? new ItemStack(item) : null;
                        }
                    }
                }
            }

            if (isBurning() && canSmelt()) {
                ++furnaceCookTime;

                if (furnaceCookTime == 200) {
                    furnaceCookTime = 0;
                    smeltItem();
                    flag = true;
                }
            } else {
                furnaceCookTime = 0;
            }

            if (burning != furnaceBurnTime > 0) {
                flag = true;
                BlockAlloyFurnace.updateBlockState(furnaceBurnTime > 0, worldObj, xCoord, yCoord, zCoord);
            }
        }

        if (flag) {
            onInventoryChanged();
        }
    }
    
    public int getCookProgressScaled(int scale) {
        return furnaceCookTime * scale / 200;
    }
    
    public int getBurnTimeRemainingScaled(int scale) {
        if (itemBurnTime == 0) {
            itemBurnTime = 200;
        }
        
        return furnaceBurnTime * scale / itemBurnTime;
    }
    
    public boolean isBurning() {
        return furnaceBurnTime > 0;
    }
    
    private boolean canSmelt() {
        if (furnaceItemStacks[0] == null || furnaceItemStacks[1] == null) {
            return false;
        } else {
            ItemStack itemStack = AlloyFurnaceRecipes.getSmeltingResult(new ItemStack[] { furnaceItemStacks[0], furnaceItemStacks[1] });
            if (itemStack == null) return false;
            if (furnaceItemStacks[3] == null) return true;
            if (!furnaceItemStacks[3].isItemEqual(itemStack)) return false;
            int result = furnaceItemStacks[3].stackSize + itemStack.stackSize;
            return (result <= getInventoryStackLimit() && result <= itemStack.getMaxStackSize());
        }
    }
    
    public void smeltItem() {
        if (canSmelt()) {
            ItemStack itemStack = AlloyFurnaceRecipes.getSmeltingResult(new ItemStack[] { furnaceItemStacks[0], furnaceItemStacks[1] });

            if (furnaceItemStacks[3] == null) {
                furnaceItemStacks[3] = itemStack.copy();
            } else if (furnaceItemStacks[3].isItemEqual(itemStack)) {
                furnaceItemStacks[3].stackSize += itemStack.stackSize;
            }

            furnaceItemStacks[0].stackSize -= AlloyFurnaceRecipes.getRequiredAmountBySlot(new ItemStack[] { furnaceItemStacks[0], furnaceItemStacks[1] }, 0);
            furnaceItemStacks[1].stackSize -= AlloyFurnaceRecipes.getRequiredAmountBySlot(new ItemStack[] { furnaceItemStacks[0], furnaceItemStacks[1] }, 1);

            if (furnaceItemStacks[0].stackSize <= 0) {
                furnaceItemStacks[0] = null;
            }
            
            if (furnaceItemStacks[1].stackSize <= 0) {
                furnaceItemStacks[1] = null;
            }
        }
    }
    
    
    @Override
    public void writeToNBT(NBTTagCompound compoundTag1) {
        super.writeToNBT(compoundTag1);
        compoundTag1.setShort("BurnTime", (short) furnaceBurnTime);
        compoundTag1.setShort("CookTime", (short) furnaceCookTime);
        NBTTagList listTag = new NBTTagList();
        
        for (int i = 0; i < furnaceItemStacks.length; i++) {
            if (furnaceItemStacks[i] != null) {
                NBTTagCompound compoundTag2 = new NBTTagCompound();
                compoundTag2.setByte("Slot", (byte) i);
                furnaceItemStacks[i].writeToNBT(compoundTag2);
                listTag.appendTag(compoundTag2);
            }
        }
        
        compoundTag1.setTag("Items", listTag);
    }
    
    @Override
    public void readFromNBT(NBTTagCompound compoundTag1) {
        super.readFromNBT(compoundTag1);
        NBTTagList listTag = compoundTag1.getTagList("Items");
        furnaceItemStacks = new ItemStack[getSizeInventory()];
        
        for (int i = 0; i < listTag.tagCount(); i++) {
            NBTTagCompound compoundTag2 = (NBTTagCompound) listTag.tagAt(i);
            byte slot = compoundTag2.getByte("Slot");
            if (slot >= 0 && slot < furnaceItemStacks.length) {
                furnaceItemStacks[slot] = ItemStack.loadItemStackFromNBT(compoundTag2);
            }
        }
        
        furnaceBurnTime = compoundTag1.getShort("BurnTime");
        furnaceCookTime = compoundTag1.getShort("CookTime");
        itemBurnTime = TileEntityFurnace.getItemBurnTime(furnaceItemStacks[2]);
    }

    @Override
    public boolean isInvNameLocalized() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isStackValidForSlot(int i, ItemStack itemstack) {
        // TODO Auto-generated method stub
        return false;
    }
}
