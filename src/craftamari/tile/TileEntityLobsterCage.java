package craftamari.tile;

import java.util.Random;

import net.minecraft.src.BiomeGenBase;
import net.minecraft.src.Block;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.IInventory;
import net.minecraft.src.Item;
import net.minecraft.src.ItemStack;
import net.minecraft.src.NBTTagCompound;
import net.minecraft.src.NBTTagList;
import net.minecraft.src.TileEntity;

public class TileEntityLobsterCage extends TileEntity implements IInventory {
    private ItemStack[] cageItemStacks = new ItemStack[9];
    private Random random = new Random();
    
    @Override
    public int getSizeInventory() {
        return cageItemStacks.length;
    }

    @Override
    public ItemStack getStackInSlot(int slot) {
        return cageItemStacks[slot];
    }

    @Override
    public ItemStack decrStackSize(int slot, int amount) {
        if (cageItemStacks[slot] != null) {
            ItemStack itemStack;
            
            if (cageItemStacks[slot].stackSize <= amount) {
                itemStack = cageItemStacks[slot];
                cageItemStacks[slot] = null;
                return itemStack;
            } else {
                itemStack = cageItemStacks[slot].splitStack(amount);
                
                if (cageItemStacks[slot].stackSize == 0) {
                    cageItemStacks[slot] = null;
                }
                
                return itemStack;
            }
        }
        
        return null;
    }

    @Override
    public ItemStack getStackInSlotOnClosing(int slot) {
        if (cageItemStacks[slot] != null) {
            ItemStack itemStack = cageItemStacks[slot];
            cageItemStacks[slot] = null;
            return itemStack;
        }
        
        return null;
    }

    @Override
    public void setInventorySlotContents(int slot, ItemStack itemStack) {
        cageItemStacks[slot] = itemStack;
        
        if (itemStack != null && itemStack.stackSize > getInventoryStackLimit()) {
            itemStack.stackSize = getInventoryStackLimit();
        }
    }

    @Override
    public String getInvName() {
        return "container.crate";
    }

    @Override
    public int getInventoryStackLimit() {
        return 1;
    }

    @Override
    public boolean isUseableByPlayer(EntityPlayer player) {
        return worldObj.getBlockTileEntity(xCoord, yCoord, zCoord) != this ? false : player.getDistanceSq(xCoord + 0.5D, yCoord + 0.5D, zCoord + 0.5D) <= 64.0D;
    }

    @Override
    public void openChest() {}

    @Override
    public void closeChest() {}
    
    @Override
    public void updateEntity() {
        if (!worldObj.isRemote) {
            if (random.nextFloat() < 0.01F && random.nextInt(100) == 0) {
                if (worldObj.getBiomeGenForCoords(xCoord, zCoord).biomeID == BiomeGenBase.ocean.biomeID) {
                    if (worldObj.getBlockId(xCoord, yCoord + 1, zCoord) == Block.waterStill.blockID && worldObj.getBlockLightValue(xCoord, yCoord + 1, zCoord) == 0) {
                        int slot = -1;
                        int count = 1;
        
                        for (int i = 0; i < cageItemStacks.length; ++i) {
                            if (cageItemStacks[i] == null && random.nextInt(count++) == 0) {
                                slot = i;
                            }
                        }
        
                        if (slot >= 0) {
                            if (random.nextInt(200) == 0) {
                                cageItemStacks[slot] = new ItemStack(Item.fishRaw);
                            } else {
                                cageItemStacks[slot] = new ItemStack(Item.lobsterRaw);
                            }
                        }
                    }
                }
            }
        }
    }
    
    @Override
    public void writeToNBT(NBTTagCompound compoundTag1) {
        super.writeToNBT(compoundTag1);
        NBTTagList listTag = new NBTTagList();
        
        for (int i = 0; i < cageItemStacks.length; i++) {
            if (cageItemStacks[i] != null) {
                NBTTagCompound compoundTag2 = new NBTTagCompound();
                compoundTag2.setByte("Slot", (byte) i);
                cageItemStacks[i].writeToNBT(compoundTag2);
                listTag.appendTag(compoundTag2);
            }
        }
        
        compoundTag1.setTag("Items", listTag);
    }
    
    @Override
    public void readFromNBT(NBTTagCompound compoundTag1) {
        super.readFromNBT(compoundTag1);
        NBTTagList listTag = compoundTag1.getTagList("Items");
        cageItemStacks = new ItemStack[getSizeInventory()];
        
        for (int i = 0; i < listTag.tagCount(); i++) {
            NBTTagCompound compoundTag2 = (NBTTagCompound) listTag.tagAt(i);
            byte slot = compoundTag2.getByte("Slot");
            if (slot >= 0 && slot < cageItemStacks.length) {
                cageItemStacks[slot] = ItemStack.loadItemStackFromNBT(compoundTag2);
            }
        }
    }

    @Override
    public boolean isInvNameLocalized() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isStackValidForSlot(int i, ItemStack itemstack) {
        // TODO Auto-generated method stub
        return false;
    }
}
