package craftamari.block;

import java.util.Random;

import craftamari.tile.TileEntityCrate;

import net.minecraft.src.BlockContainer;
import net.minecraft.src.CreativeTabs;
import net.minecraft.src.EntityItem;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.ItemStack;
import net.minecraft.src.Material;
import net.minecraft.src.NBTTagCompound;
import net.minecraft.src.TileEntity;
import net.minecraft.src.World;

public class BlockCrate extends BlockContainer {
    private Random random = new Random();
    
    public BlockCrate(int id) {
        super(id, Material.wood);
        this.setCreativeTab(CreativeTabs.tabDecorations);
    }
    
    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ) {
        if (!world.isRemote) {
            TileEntityCrate tileEntityCrate = (TileEntityCrate) world.getBlockTileEntity(x, y, z);
            
            if (tileEntityCrate != null) {
                player.displayGUICrate(tileEntityCrate);
            }
            
            return true;
        }

        return true;
    }
    
    @Override
    public void breakBlock(World world, int x, int y, int z, int id, int metadata) {
        TileEntityCrate tileEntity = (TileEntityCrate) world.getBlockTileEntity(x, y, z);
        
        if (tileEntity != null) {
            for (int i = 0; i < tileEntity.getSizeInventory(); ++i) {
                ItemStack itemStack = tileEntity.getStackInSlot(i);

                if (itemStack != null) {
                    float var10 = random.nextFloat() * 0.8F + 0.1F;
                    float var11 = random.nextFloat() * 0.8F + 0.1F;
                    float var12 = random.nextFloat() * 0.8F + 0.1F;

                    while (itemStack.stackSize > 0) {
                        int var13 = random.nextInt(21) + 10;

                        if (var13 > itemStack.stackSize) {
                            var13 = itemStack.stackSize;
                        }

                        itemStack.stackSize -= var13;
                        EntityItem entityItem = new EntityItem(world, (x + var10), (y + var11), (z + var12), new ItemStack(itemStack.itemID, var13, itemStack.getItemDamage()));

                        if (itemStack.hasTagCompound()) {
                            entityItem.getEntityItem().setTagCompound((NBTTagCompound) itemStack.getTagCompound().copy());
                        }

                        float var15 = 0.05F;
                        entityItem.motionX = ((float) random.nextGaussian() * var15);
                        entityItem.motionY = ((float) random.nextGaussian() * var15 + 0.2F);
                        entityItem.motionZ = ((float) random.nextGaussian() * var15);
                        world.spawnEntityInWorld(entityItem);
                    }
                }
            }
        }
        
        super.breakBlock(world, x, y, z, id, metadata);
    }

    @Override
    public TileEntity createNewTileEntity(World var1) {
        return new TileEntityCrate();
    }
}
