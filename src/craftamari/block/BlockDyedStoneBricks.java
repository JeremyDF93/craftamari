package craftamari.block;

import java.util.List;

import net.minecraft.src.Block;
import net.minecraft.src.BlockCloth;
import net.minecraft.src.CreativeTabs;
import net.minecraft.src.Icon;
import net.minecraft.src.IconRegister;
import net.minecraft.src.ItemDye;
import net.minecraft.src.ItemStack;
import net.minecraft.src.Material;

public class BlockDyedStoneBricks extends Block {
    private Icon[] iconArray;

    public BlockDyedStoneBricks(int id) {
        super(id, Material.rock);
        setCreativeTab(CreativeTabs.tabBlock);
    }

    public Icon getIcon(int id, int metadata) {
        return this.iconArray[metadata % this.iconArray.length];
    }

    @Override
    public int damageDropped(int metadata) {
        return metadata;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public void getSubBlocks(int unknown, CreativeTabs tab, List subItems) {
        for (int i = 0; i < 16; i++) {
            subItems.add(new ItemStack(this, 1, i));
        }
    }

    public void registerIcons(IconRegister par1IconRegister) {
        this.iconArray = new Icon[16];

        for (int i = 0; i < this.iconArray.length; ++i) {
            this.iconArray[i] = par1IconRegister.registerIcon("brick_" + ItemDye.dyeColorNames[BlockCloth.getBlockFromDye(i)]);
        }
    }
}