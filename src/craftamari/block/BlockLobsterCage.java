package craftamari.block;

import java.util.Random;

import craftamari.tile.TileEntityLobsterCage;

import net.minecraft.src.BlockContainer;
import net.minecraft.src.BlockSand;
import net.minecraft.src.CreativeTabs;
import net.minecraft.src.EntityFallingSand;
import net.minecraft.src.EntityItem;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.Icon;
import net.minecraft.src.IconRegister;
import net.minecraft.src.ItemStack;
import net.minecraft.src.Material;
import net.minecraft.src.NBTTagCompound;
import net.minecraft.src.TileEntity;
import net.minecraft.src.World;

public class BlockLobsterCage extends BlockContainer {
    public static boolean fallInstantly = false;
    
    private Random random = new Random();

    private Icon blockSideIcon;
    private Icon blockBottomIcon;

    public BlockLobsterCage(int id) {
        super(id, Material.iron);
        this.setLightOpacity(1);
        this.setCreativeTab(CreativeTabs.tabBlock);
    }

    @Override
    public void onBlockAdded(World world, int x, int y, int z) {
        world.scheduleBlockUpdate(x, y, z, blockID, tickRate(world));
    }

    @Override
    public void onNeighborBlockChange(World world, int x, int y, int z, int id) {
        world.scheduleBlockUpdate(x, y, z, blockID, tickRate(world));
    }

    @Override
    public void updateTick(World world, int x, int y, int z, Random random) {
        if (!world.isRemote) {
            tryToFall(world, x, y, z);
        }
    }

    private void tryToFall(World world, int x, int y, int z) {
        if (BlockSand.canFallBelow(world, x, y - 1, z) && y >= 0) {
            byte var8 = 32;

            if (!fallInstantly && world.checkChunksExist(x - var8, y - var8, z - var8, x + var8, y + var8, z + var8)) {
                if (!world.isRemote) {
                    EntityFallingSand entityFallingSand = new EntityFallingSand(world, (x + 0.5F), (y + 0.5F), (z + 0.5F), blockID, world.getBlockMetadata(x, y, z));
                    onStartFalling(entityFallingSand);
                    world.spawnEntityInWorld(entityFallingSand);
                }
            } else {
                world.setBlock(x, y, z, 0);

                while (BlockSand.canFallBelow(world, x, y - 1, z) && y > 0) {
                    --y;
                }

                if (y > 0) {
                    world.setBlock(x, y, z, this.blockID);
                }
            }
        }
    }
    
    protected void onStartFalling(EntityFallingSand par1EntityFallingSand) {}
    
    @Override
    public int tickRate(World world) {
        return 5;
    }

    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ) {
        if (!world.isRemote) {
            TileEntityLobsterCage tileEntityLobsterCage = (TileEntityLobsterCage) world.getBlockTileEntity(x, y, z);
            
            if (tileEntityLobsterCage != null) {
                player.displayGUILobsterCage(tileEntityLobsterCage);
            }
            
            return true;
        }

        return true;
    }

    @Override
    public void breakBlock(World world, int x, int y, int z, int id, int metadata) {
        TileEntityLobsterCage tileEntity = (TileEntityLobsterCage) world.getBlockTileEntity(x, y, z);

        if (tileEntity != null) {
            for (int i = 0; i < tileEntity.getSizeInventory(); ++i) {
                ItemStack itemStack = tileEntity.getStackInSlot(i);

                if (itemStack != null) {
                    float var10 = random.nextFloat() * 0.8F + 0.1F;
                    float var11 = random.nextFloat() * 0.8F + 0.1F;
                    float var12 = random.nextFloat() * 0.8F + 0.1F;

                    while (itemStack.stackSize > 0) {
                        int var13 = random.nextInt(21) + 10;

                        if (var13 > itemStack.stackSize) {
                            var13 = itemStack.stackSize;
                        }

                        itemStack.stackSize -= var13;
                        EntityItem entityItem = new EntityItem(world, (x + var10), (y + var11), (z + var12), new ItemStack(itemStack.itemID, var13, itemStack.getItemDamage()));

                        if (itemStack.hasTagCompound()) {
                            entityItem.getEntityItem().setTagCompound((NBTTagCompound) itemStack.getTagCompound().copy());
                        }

                        float var15 = 0.05F;
                        entityItem.motionX = ((float) random.nextGaussian() * var15);
                        entityItem.motionY = ((float) random.nextGaussian() * var15 + 0.2F);
                        entityItem.motionZ = ((float) random.nextGaussian() * var15);
                        world.spawnEntityInWorld(entityItem);
                    }
                }
            }
        }

        super.breakBlock(world, x, y, z, id, metadata);
    }

    @Override
    public TileEntity createNewTileEntity(World world) {
        return new TileEntityLobsterCage();
    }

    @Override
    public boolean isOpaqueCube() {
        return false;
    }
    
    @Override
    public Icon getIcon(int side, int metadata) {
        return side == 1 ? blockIcon : (side != 0 ? blockSideIcon : blockBottomIcon);
    }
    
    @Override
    public void registerIcons(IconRegister iconRegister) {
        blockIcon = iconRegister.registerIcon("lobsterCage_top");
        blockSideIcon = iconRegister.registerIcon("lobsterCage_side");
        blockBottomIcon = iconRegister.registerIcon("lobsterCage_bottom");
    }
}
