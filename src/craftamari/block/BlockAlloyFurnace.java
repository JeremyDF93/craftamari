package craftamari.block;

import java.util.Random;

import craftamari.tile.TileEntityAlloyFurnace;

import net.minecraft.src.Block;
import net.minecraft.src.BlockContainer;
import net.minecraft.src.CreativeTabs;
import net.minecraft.src.EntityItem;
import net.minecraft.src.EntityLiving;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.Icon;
import net.minecraft.src.IconRegister;
import net.minecraft.src.ItemStack;
import net.minecraft.src.Material;
import net.minecraft.src.MathHelper;
import net.minecraft.src.NBTTagCompound;
import net.minecraft.src.TileEntity;
import net.minecraft.src.World;

public class BlockAlloyFurnace extends BlockContainer {
    private static boolean keepFurnaceInventory = false;

    private final boolean isActive;

    private Random random = new Random();
    
    private Icon blockFrontIcon;

    public BlockAlloyFurnace(int id, boolean isActive) {
        super(id, Material.rock);
        this.isActive = isActive;
        
        if (isActive) {
            setLightValue(0.875F);
        } else {
            setCreativeTab(CreativeTabs.tabDecorations);
        }
    }

    @Override
    public int idDropped(int metadata, Random random, int id) {
        return Block.alloyFurnaceIdle.blockID;
    }

    @Override
    public void onBlockAdded(World world, int x, int y, int z) {
        super.onBlockAdded(world, x, y, z);
        this.setDefaultDirection(world, x, y, z);
    }

    private void setDefaultDirection(World world, int x, int y, int z) {
        if (!world.isRemote) {
            int blockID1 = world.getBlockId(x, y, z - 1);
            int blockID2 = world.getBlockId(x, y, z + 1);
            int blockID3 = world.getBlockId(x - 1, y, z);
            int blockID4 = world.getBlockId(x + 1, y, z);
            byte metadata = 3;

            if (Block.opaqueCubeLookup[blockID1] && !Block.opaqueCubeLookup[blockID2]) {
                metadata = 3;
            }

            if (Block.opaqueCubeLookup[blockID2] && !Block.opaqueCubeLookup[blockID1]) {
                metadata = 2;
            }

            if (Block.opaqueCubeLookup[blockID3] && !Block.opaqueCubeLookup[blockID4]) {
                metadata = 5;
            }

            if (Block.opaqueCubeLookup[blockID4] && !Block.opaqueCubeLookup[blockID3]) {
                metadata = 4;
            }

            world.setBlockMetadataWithNotify(x, y, z, metadata, 2);
        }
    }

    @Override
    public void randomDisplayTick(World world, int x, int y, int z, Random random) {
        if (this.isActive) {
            int metadata = world.getBlockMetadata(x, y, z);
            float var7 = x + 0.5F;
            float var8 = y + 0.0F + random.nextFloat() * 6.0F / 16.0F;
            float var9 = z + 0.5F;
            float var10 = 0.52F;
            float var11 = random.nextFloat() * 0.6F - 0.3F;

            if (metadata == 4) {
                world.spawnParticle("smoke", (var7 - var10), var8, (var9 + var11), 0.0D, 0.0D, 0.0D);
                world.spawnParticle("flame", (var7 - var10), var8, (var9 + var11), 0.0D, 0.0D, 0.0D);
            } else if (metadata == 5) {
                world.spawnParticle("smoke", (var7 + var10), var8, (var9 + var11), 0.0D, 0.0D, 0.0D);
                world.spawnParticle("flame", (var7 + var10), var8, (var9 + var11), 0.0D, 0.0D, 0.0D);
            } else if (metadata == 2) {
                world.spawnParticle("smoke", (var7 + var11), var8, (var9 - var10), 0.0D, 0.0D, 0.0D);
                world.spawnParticle("flame", (var7 + var11), var8, (var9 - var10), 0.0D, 0.0D, 0.0D);
            } else if (metadata == 3) {
                world.spawnParticle("smoke", (var7 + var11), var8, (var9 + var10), 0.0D, 0.0D, 0.0D);
                world.spawnParticle("flame", (var7 + var11), var8, (var9 + var10), 0.0D, 0.0D, 0.0D);
            }
        }
    }

    public static void updateBlockState(boolean isActive, World world, int x, int y, int z) {
        int metadata = world.getBlockMetadata(x, y, z);
        TileEntity tileEntity = world.getBlockTileEntity(x, y, z);
        keepFurnaceInventory = true;

        if (isActive) {
            world.setBlock(x, y, z, Block.alloyFurnaceActive.blockID);
        } else {
            world.setBlock(x, y, z, Block.alloyFurnaceIdle.blockID);
        }

        keepFurnaceInventory = false;
        world.setBlockMetadataWithNotify(x, y, z, metadata, 2);

        if (tileEntity != null) {
            tileEntity.validate();
            world.setBlockTileEntity(x, y, z, tileEntity);
        }
    }

    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ) {
        if (!world.isRemote) {
            TileEntityAlloyFurnace tileEntityAlloyFurnace = (TileEntityAlloyFurnace) world.getBlockTileEntity(x, y, z);
            
            if (tileEntityAlloyFurnace != null) {
                player.displayGUIAlloyFurnace(tileEntityAlloyFurnace);
            }
            
            return true;
        }

        return true;
    }

    @Override
    public void onBlockPlacedBy(World world, int x, int y, int z, EntityLiving entityLiving, ItemStack itemStack) {
        int side = MathHelper.floor_double((entityLiving.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3;

        if (side == 0) {
            world.setBlockMetadataWithNotify(x, y, z, 2, 2);
        }

        if (side == 1) {
            world.setBlockMetadataWithNotify(x, y, z, 5, 2);
        }

        if (side == 2) {
            world.setBlockMetadataWithNotify(x, y, z, 3, 2);
        }

        if (side == 3) {
            world.setBlockMetadataWithNotify(x, y, z, 4, 2);
        }
    }

    @Override
    public void breakBlock(World world, int x, int y, int z, int id, int metadata) {
        if (!keepFurnaceInventory) {
            TileEntityAlloyFurnace tileEntity = (TileEntityAlloyFurnace) world.getBlockTileEntity(x, y, z);

            if (tileEntity != null) {
                for (int i = 0; i < tileEntity.getSizeInventory(); ++i) {
                    ItemStack itemStack = tileEntity.getStackInSlot(i);

                    if (itemStack != null) {
                        float var10 = random.nextFloat() * 0.8F + 0.1F;
                        float var11 = random.nextFloat() * 0.8F + 0.1F;
                        float var12 = random.nextFloat() * 0.8F + 0.1F;

                        while (itemStack.stackSize > 0) {
                            int var13 = random.nextInt(21) + 10;

                            if (var13 > itemStack.stackSize) {
                                var13 = itemStack.stackSize;
                            }

                            itemStack.stackSize -= var13;
                            EntityItem entityItem = new EntityItem(world, (x + var10), (y + var11), (z + var12), new ItemStack(itemStack.itemID, var13, itemStack.getItemDamage()));

                            if (itemStack.hasTagCompound()) {
                                entityItem.getEntityItem().setTagCompound((NBTTagCompound) itemStack.getTagCompound().copy());
                            }

                            float var15 = 0.05F;
                            entityItem.motionX = ((float) random.nextGaussian() * var15);
                            entityItem.motionY = ((float) random.nextGaussian() * var15 + 0.2F);
                            entityItem.motionZ = ((float) random.nextGaussian() * var15);
                            world.spawnEntityInWorld(entityItem);
                        }
                    }
                }
            }
        }

        super.breakBlock(world, x, y, z, id, metadata);
    }

    @Override
    public TileEntity createNewTileEntity(World world) {
        return new TileEntityAlloyFurnace();
    }
    
    @Override
    public Icon getIcon(int side, int metadata) {
        if (metadata == 0) metadata = 3;
        return side != metadata ? blockIcon : blockFrontIcon;
    }
    
    @Override
    public void registerIcons(IconRegister iconRegister) {
        blockIcon = iconRegister.registerIcon("alloyFurnace_side");
        blockFrontIcon = iconRegister.registerIcon(isActive ? "alloyFurnace_front_lit" : "alloyFurnace_front");
    }
}
