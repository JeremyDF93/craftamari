package net.minecraft.src;

public class RecipesArmor {
    private String[][] recipePatterns = new String[][] {{"XXX", "X X"}, {"X X", "XXX", "XXX"}, {"XXX", "X X", "X X"}, {"X X", "X X"}};
    private Object[][] recipeItems;

    public RecipesArmor() {
        this.recipeItems = new Object[][] {
                { Item.leather, Block.fire, Item.ingotIron, Item.diamond, Item.ingotGold, Item.ingotBronze, Item.ingotSteel, Item.ingotWhiteSteel, Item.ingotBlackSteel, Item.ingotMithril, Item.ingotAdamant, Item.ingotRune },
                { Item.helmetLeather, Item.helmetChain, Item.helmetIron, Item.helmetDiamond, Item.helmetGold, Item.helmetBronze, Item.helmetSteel, Item.helmetWhiteSteel, Item.helmetBlackSteel, Item.helmetMithril, Item.helmetAdamant, Item.helmetRune },
                { Item.plateLeather, Item.plateChain, Item.plateIron, Item.plateDiamond, Item.plateGold, Item.chestplateBronze, Item.chestplateSteel, Item.chestplateWhiteSteel, Item.chestplateBlackSteel, Item.chestplateMithril, Item.chestplateAdamant, Item.chestplateRune },
                { Item.legsLeather, Item.legsChain, Item.legsIron, Item.legsDiamond, Item.legsGold, Item.leggingsBronze, Item.leggingsSteel,Item.leggingsWhiteSteel, Item.leggingsBlackSteel,  Item.leggingsMithril, Item.leggingsAdamant, Item.leggingsRune },
                { Item.bootsLeather, Item.bootsChain, Item.bootsIron, Item.bootsDiamond, Item.bootsGold, Item.bootsBronze, Item.bootsSteel, Item.bootsWhiteSteel, Item.bootsBlackSteel, Item.bootsMithril, Item.bootsAdamant, Item.bootsRune } };
    }

    /**
     * Adds the armor recipes to the CraftingManager.
     */
    public void addRecipes(CraftingManager craftingManager) {
        for (int var2 = 0; var2 < this.recipeItems[0].length; ++var2) {
            Object var3 = this.recipeItems[0][var2];

            for (int var4 = 0; var4 < this.recipeItems.length - 1; ++var4) {
                Item var5 = (Item)this.recipeItems[var4 + 1][var2];
                craftingManager.addRecipe(new ItemStack(var5), new Object[] {this.recipePatterns[var4], 'X', var3});
            }
        }
        
    	craftingManager.addRecipe(new ItemStack(Item.capeObsidian, 1, 0), new Object[] {"##", "##", "XX", '#', Item.obsidianSheet, 'X', Item.refinedSheet});
    }
}
