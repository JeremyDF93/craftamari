package net.minecraft.src;

import craftamari.item.ItemRing;

public class SlotRing extends Slot {
    public SlotRing(IInventory iInventory, int id, int x, int y) {
        super(iInventory, id, x, y);
        // TODO Auto-generated constructor stub
    }
    
    public boolean isItemValid(ItemStack itemStack) {
        return itemStack.getItem() instanceof ItemRing;
    }
    
    public Icon getBackgroundIconIndex() {
        return ItemRing.getIconBackground();
    }
}
