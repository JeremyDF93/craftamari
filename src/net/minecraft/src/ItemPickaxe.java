package net.minecraft.src;

public class ItemPickaxe extends ItemTool {
    private Object[][] blockHarvestLevels = { { Block.obsidian, 3 },
            { Block.blockDiamond, 2 }, { Block.oreDiamond, 2 },
            { Block.blockEmerald, 2 }, { Block.oreDiamond, 2 },
            { Block.blockGold, 2 }, { Block.oreGold, 2 },
            { Block.blockIron, 1 }, { Block.oreIron, 1 },
            { Block.blockLapis, 1 }, { Block.oreLapis, 1 },
            { Block.oreCopper, 1 }, { Block.oreTin, 1 },
            { Block.oreMithril, 2 }, { Block.blockMithril, 2 },
            { Block.oreAdamant, 3 }, { Block.blockAdamant, 3 },
            { Block.oreRune, 4 }, { Block.blockRune, 4 },
            { Block.blockBronze, 2 } };

    /** an array of the blocks this pickaxe is effective against */
    private static Block[] blocksEffectiveAgainst = new Block[] {
            Block.cobblestone, Block.stoneDoubleSlab, Block.stoneSingleSlab,
            Block.stone, Block.sandStone, Block.cobblestoneMossy,
            Block.oreIron, Block.blockIron, Block.oreCoal, Block.blockGold,
            Block.oreGold, Block.oreDiamond, Block.blockDiamond, Block.ice,
            Block.netherrack, Block.oreLapis, Block.blockLapis,
            Block.oreRedstone, Block.oreRedstoneGlowing, Block.rail,
            Block.railDetector, Block.railPowered, Block.railActivator,
            Block.oreCopper, Block.oreTin, Block.oreMithril,
            Block.oreAdamant, Block.oreRune, Block.blockBronze,
            Block.blockMithril, Block.blockAdamant, Block.blockRune };

    protected ItemPickaxe(int par1, EnumToolMaterial par2EnumToolMaterial) {
        super(par1, 2, par2EnumToolMaterial, blocksEffectiveAgainst);
    }

    /**
     * Returns if the item (tool) can harvest results from the block type.
     */
    public boolean canHarvestBlock(Block par1Block) {
        for (int i = 0; i < blockHarvestLevels.length; i++) {
            Block block = (Block) blockHarvestLevels[i][0];
            int harvestLevel = (Integer) blockHarvestLevels[i][1];
            
            if (par1Block == block) {
                return this.toolMaterial.getHarvestLevel() >= harvestLevel;
            }
        }
        
        if (par1Block.blockMaterial == Material.rock || par1Block.blockMaterial == Material.iron || par1Block.blockMaterial == Material.anvil) {
            return true;
        }
        
        return false;
    }

    /**
     * Returns the strength of the stack against a given block. 1.0F base, (Quality+1)*2 if correct blocktype, 1.5F if
     * sword
     */
    public float getStrVsBlock(ItemStack par1ItemStack, Block par2Block) {
        return par2Block != null && (par2Block.blockMaterial == Material.iron || par2Block.blockMaterial == Material.anvil || par2Block.blockMaterial == Material.rock) ? this.efficiencyOnProperMaterial : super.getStrVsBlock(par1ItemStack, par2Block);
    }
}
