package net.minecraft.src;

public class EntityItemFrame extends EntityHanging {

    /** Chance for this item frame's item to drop from the frame. */
    private float itemDropChance = 1.0F;

    public EntityItemFrame(World par1World) {
        super(par1World);
    }

    public EntityItemFrame(World world, int x, int y, int z, int dir) {
        super(world, x, y, z, dir);
        this.setDirection(dir);
    }

    protected void entityInit() {
        this.getDataWatcher().addObjectByDataType(2, 5);
        this.getDataWatcher().addObject(3, Byte.valueOf((byte)0));
        this.getDataWatcher().addObject(4, Byte.valueOf((byte)0)); // Craftamari
    }

    public int func_82329_d() {
        return 9;
    }

    public int func_82330_g() {
        return 9;
    }

    /**
     * Checks if the entity is in range to render by using the past in distance and comparing it to its average edge
     * length * 64 * renderDistanceWeight Args: distance
     */
    public boolean isInRangeToRenderDist(double par1) {
        double var3 = 16.0D;
        var3 *= 64.0D * this.renderDistanceWeight;
        return par1 < var3 * var3;
    }

    /**
     * Drop the item currently on this item frame.
     */
    public void dropItemStack() {
    	// Craftamari
    	if(!this.isFrameHidden()) {
    		this.entityDropItem(new ItemStack(Item.itemFrame), 0.0F);
    	}
    	// !Craftamari
        ItemStack var1 = this.getDisplayedItem();

        if (var1 != null && this.rand.nextFloat() < this.itemDropChance) {
            var1 = var1.copy();
            var1.setItemFrame((EntityItemFrame)null);
            this.entityDropItem(var1, 0.0F);
        }
    }

    public ItemStack getDisplayedItem() {
        return this.getDataWatcher().getWatchableObjectItemStack(2);
    }
    
    // Craftamari
    public void hideFrame(boolean hide) {
    	this.getDataWatcher().updateObject(4, Byte.valueOf((byte) (hide ? 1 : 0)));
    }
    
    public boolean isFrameHidden() {
    	return this.getDataWatcher().getWatchableObjectByte(4)==1;
    }
    // !Craftamari

    public void setDisplayedItem(ItemStack par1ItemStack) {
        par1ItemStack = par1ItemStack.copy();
        par1ItemStack.stackSize = 1;
        par1ItemStack.setItemFrame(this);
        this.getDataWatcher().updateObject(2, par1ItemStack);
        this.getDataWatcher().setObjectWatched(2);
    }

    /**
     * Return the rotation of the item currently on this frame.
     */
    public int getRotation() {
        return this.getDataWatcher().getWatchableObjectByte(3);
    }

    public void setItemRotation(int par1) {
        this.getDataWatcher().updateObject(3, Byte.valueOf((byte)(par1 % 4)));
    }

    /**
     * (abstract) Protected helper method to write subclass entity data to NBT.
     */
    public void writeEntityToNBT(NBTTagCompound par1NBTTagCompound) {
        if (this.getDisplayedItem() != null) {
            par1NBTTagCompound.setCompoundTag("Item", this.getDisplayedItem().writeToNBT(new NBTTagCompound()));
            par1NBTTagCompound.setByte("ItemRotation", (byte)this.getRotation());
            par1NBTTagCompound.setFloat("ItemDropChance", this.itemDropChance);
        }
        par1NBTTagCompound.setBoolean("Invis", this.isFrameHidden()); // Craftamari

        super.writeEntityToNBT(par1NBTTagCompound);
    }

    /**
     * (abstract) Protected helper method to read subclass entity data from NBT.
     */
    public void readEntityFromNBT(NBTTagCompound par1NBTTagCompound) {
        NBTTagCompound var2 = par1NBTTagCompound.getCompoundTag("Item");

        if (var2 != null && !var2.hasNoTags()) {
            this.setDisplayedItem(ItemStack.loadItemStackFromNBT(var2));
            this.setItemRotation(par1NBTTagCompound.getByte("ItemRotation"));

            if (par1NBTTagCompound.hasKey("ItemDropChance")) {
                this.itemDropChance = par1NBTTagCompound.getFloat("ItemDropChance");
            }
            // Craftamari
            if (par1NBTTagCompound.hasKey("Invis")) {
            	if(par1NBTTagCompound.getBoolean("Invis")) {
            		this.getDataWatcher().updateObject(4, Byte.valueOf((byte)1));
            	}
            }
            // !Craftamari
        }

        super.readEntityFromNBT(par1NBTTagCompound);
    }

    /**
     * Called when a player interacts with a mob. e.g. gets milk from a cow, gets into the saddle on a pig.
     */
    // Craftamari start
    public boolean interact(EntityPlayer player) {
        if (this.getDisplayedItem() == null && !this.isFrameHidden()) {
            ItemStack itemStack = player.getHeldItem();

            if (itemStack != null && !this.worldObj.isRemote) {
                this.setDisplayedItem(itemStack);

                if (!player.capabilities.isCreativeMode && --itemStack.stackSize <= 0) {
                    player.inventory.setInventorySlotContents(player.inventory.currentItem, (ItemStack)null);
                }
            }
        } else if (!this.worldObj.isRemote) {
        	if(player.capabilities.isCreativeMode && !this.isFrameHidden()) {
        		this.setItemRotation(this.getRotation() + 1);
        	}
        }

        return true;
    }
 // Craftamari end
}
