package net.minecraft.src;

import net.minecraft.src.ItemStack;

public class RecipesJewelry {
    private Object[][] recipeItems = new Object[][] {
            { new ItemStack(Item.ring, 1, 1), new ItemStack(Item.ring, 1, 0), Item.sapphire },
            { new ItemStack(Item.ring, 1, 2), new ItemStack(Item.ring, 1, 0), Item.emerald },
            { new ItemStack(Item.ring, 1, 3), new ItemStack(Item.ring, 1, 0), Item.ruby },
            { new ItemStack(Item.ring, 1, 4), new ItemStack(Item.ring, 1, 0), Item.diamond },
            { new ItemStack(Item.ring, 1, 5), new ItemStack(Item.ring, 1, 0), Item.dragonstone },
            { new ItemStack(Item.ring, 1, 6), new ItemStack(Item.ring, 1, 0), Item.onyx },
            { new ItemStack(Item.amulet, 1, 1), new ItemStack(Item.amulet, 1, 0), Item.sapphire },
            { new ItemStack(Item.amulet, 1, 2), new ItemStack(Item.amulet, 1, 0), Item.emerald },
            { new ItemStack(Item.amulet, 1, 3), new ItemStack(Item.amulet, 1, 0), Item.ruby },
            { new ItemStack(Item.amulet, 1, 4), new ItemStack(Item.amulet, 1, 0), Item.diamond },
            { new ItemStack(Item.amulet, 1, 5), new ItemStack(Item.amulet, 1, 0), Item.dragonstone },
            { new ItemStack(Item.amulet, 1, 6), new ItemStack(Item.amulet, 1, 0), Item.onyx }};

    public void addRecipes(CraftingManager craftingManager) {
        Object[][] objects = this.recipeItems;
        int size = objects.length;

        for (int i = 0; i < size; ++i) {
            Object[] object = objects[i];
            craftingManager.addShapelessRecipe((ItemStack) object[0], object[1], object[2]);
        }
    }
}