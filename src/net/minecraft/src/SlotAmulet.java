package net.minecraft.src;

import craftamari.item.ItemAmulet;

public class SlotAmulet extends Slot {
    public SlotAmulet(IInventory iInventory, int id, int x, int y) {
        super(iInventory, id, x, y);
        // TODO Auto-generated constructor stub
    }
    
    public boolean isItemValid(ItemStack itemStack) {
        return itemStack.getItem() instanceof ItemAmulet;
    }
    
    public Icon getBackgroundIconIndex() {
        return ItemAmulet.getIconBackground();
    }
}
