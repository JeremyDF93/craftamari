package net.minecraft.src;


public class RecipesDyedStoneBricks implements IRecipe {
    private ItemStack recipeOutput;
    
    @Override
    public boolean matches(InventoryCrafting var1, World var2) {
        int stoneBrickCount = 0;
        int dyePowderCount = 0;
        int dyeColor = 0;
        
        for (int i = 0; i < var1.getSizeInventory(); ++i) {
            ItemStack itemStack = var1.getStackInSlot(i);

            if (itemStack != null) {
                if (itemStack.itemID == Block.stoneBrick.blockID) {
                    stoneBrickCount++;
                } else if (itemStack.itemID == Item.dyePowder.itemID) {
                    dyePowderCount++;
                    dyeColor = BlockCloth.getBlockFromDye(itemStack.getItemDamage());
                } else {
                    return false;
                }
            }
        }
        
        if (stoneBrickCount >= 1 && dyePowderCount == 1) {
            this.recipeOutput = new ItemStack(Block.dyedStoneBrick, stoneBrickCount, dyeColor);
            return true;
        }
        
        return false;
    }

    @Override
    public ItemStack getCraftingResult(InventoryCrafting var1) {
        return this.recipeOutput.copy();
    }

    @Override
    public int getRecipeSize() {
        return 10;
    }

    @Override
    public ItemStack getRecipeOutput() {
        return this.recipeOutput;
    }
}
