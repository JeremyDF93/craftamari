package net.minecraft.src;

import craftamari.item.ItemAmulet;
import craftamari.item.ItemCape;
import craftamari.item.ItemRing;

public class ContainerPlayer extends Container {

    /** The crafting matrix inventory. */
    public InventoryCrafting craftMatrix = new InventoryCrafting(this, 2, 2);
    public IInventory craftResult = new InventoryCraftResult();

    /** Determines if inventory manipulation should be handled. */
    public boolean isLocalWorld = false;
    private final EntityPlayer thePlayer;

    public ContainerPlayer(InventoryPlayer inventoryPlayer, boolean local, EntityPlayer player) {
        this.isLocalWorld = local;
        this.thePlayer = player;
        this.addSlotToContainer(new SlotCrafting(inventoryPlayer.player, this.craftMatrix, this.craftResult, 0, 162, 36));
        int y;
        int x;

        for (y = 0; y < 2; ++y) {
            for (x = 0; x < 2; ++x) {
                this.addSlotToContainer(new Slot(this.craftMatrix, x + y * 2, 106 + x * 18, 26 + y * 18));
            }
        }
        
        this.addSlotToContainer(new SlotCape(inventoryPlayer, inventoryPlayer.getSizeInventory() - 1, 8, 17));
        this.addSlotToContainer(new SlotAmulet(inventoryPlayer, inventoryPlayer.getSizeInventory() - 2, 8, 35));
        this.addSlotToContainer(new SlotRing(inventoryPlayer, inventoryPlayer.getSizeInventory() - 3, 8, 53));

        for (y = 0; y < 4; ++y) {
            this.addSlotToContainer(new SlotArmor(this, inventoryPlayer, inventoryPlayer.getSizeInventory() - 4 - y, 26, 8 + y * 18, y));
        }

        for (y = 0; y < 3; ++y) {
            for (x = 0; x < 9; ++x) {
                this.addSlotToContainer(new Slot(inventoryPlayer, x + (y + 1) * 9, 17 + x * 18, 84 + y * 18));
            }
        }

        for (y = 0; y < 9; ++y) {
            this.addSlotToContainer(new Slot(inventoryPlayer, y, 17 + y * 18, 142));
        }

        this.onCraftMatrixChanged(this.craftMatrix);
    }

    /**
     * Callback for when the crafting matrix is changed.
     */
    public void onCraftMatrixChanged(IInventory iInventory) {
        this.craftResult.setInventorySlotContents(0, CraftingManager.getInstance().findMatchingRecipe(this.craftMatrix, this.thePlayer.worldObj));
    }

    /**
     * Callback for when the crafting gui is closed.
     */
    public void onCraftGuiClosed(EntityPlayer player) {
        super.onCraftGuiClosed(player);

        for (int i = 0; i < 4; ++i) {
            ItemStack var3 = this.craftMatrix.getStackInSlotOnClosing(i);

            if (var3 != null) {
                player.dropPlayerItem(var3);
            }
        }

        this.craftResult.setInventorySlotContents(0, (ItemStack)null);
    }

    public boolean canInteractWith(EntityPlayer par1EntityPlayer) {
        return true;
    }

    /**
     * Called when a player shift-clicks on a slot. You must override this or you will crash when someone does that.
     */
    public ItemStack transferStackInSlot(EntityPlayer player, int i) {
        ItemStack var3 = null;
        Slot slot = (Slot)this.inventorySlots.get(i);

        if (slot != null && slot.getHasStack()) {
            ItemStack var5 = slot.getStack();
            var3 = var5.copy();

            if (i == 0) {
                if (!this.mergeItemStack(var5, 12, 48, true)) {
                    return null;
                }

                slot.onSlotChange(var5, var3);
            } else if (i >= 1 && i < 8) {
                if (!this.mergeItemStack(var5, 12, 48, false)) {
                    return null;
                }
            } else if (i >= 8 && i < 12) {
                if (!this.mergeItemStack(var5, 12, 48, false)) {
                    return null;
                }
            } else if (var3.getItem() instanceof ItemArmor && !((Slot) this.inventorySlots.get(8 + ((ItemArmor) var3.getItem()).armorType)).getHasStack()) {
                int var6 = 8 + ((ItemArmor)var3.getItem()).armorType;

                if (!this.mergeItemStack(var5, var6, var6 + 1, false)) {
                    return null;
                }
            } else if (var3.getItem() instanceof ItemCape && !((Slot) this.inventorySlots.get(5)).getHasStack()) {
                if (!this.mergeItemStack(var5, 5, 6, false)) {
                    return null;
                }
            } else if (var3.getItem() instanceof ItemAmulet && !((Slot) this.inventorySlots.get(6)).getHasStack()) {
                if (!this.mergeItemStack(var5, 6, 7, false)) {
                    return null;
                }
            } else if (var3.getItem() instanceof ItemRing && !((Slot) this.inventorySlots.get(7)).getHasStack()) {
                if (!this.mergeItemStack(var5, 7, 8, false)) {
                    return null;
                }
            } else if (i >= 12 && i < 39) {
                if (!this.mergeItemStack(var5, 39, 48, false)) {
                    return null;
                }
            } else if (i >= 39 && i < 48) {
                if (!this.mergeItemStack(var5, 12, 39, false)) {
                    return null;
                }
            } else if (!this.mergeItemStack(var5, 12, 48, false)) {
                return null;
            }

            if (var5.stackSize == 0) {
                slot.putStack((ItemStack)null);
            } else {
                slot.onSlotChanged();
            }

            if (var5.stackSize == var3.stackSize) {
                return null;
            }

            slot.onPickupFromSlot(player, var5);
        }

        return var3;
    }

    public boolean func_94530_a(ItemStack itemStack, Slot slot) {
        return slot.inventory != this.craftResult && super.func_94530_a(itemStack, slot);
    }
}
