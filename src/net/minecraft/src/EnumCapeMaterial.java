package net.minecraft.src;


public enum EnumCapeMaterial {
    CLOTH(1, 15),
    OBSIDIAN(1, 10),
    LEATHER(1, 15);
    
    private int damageReductionAmount;
    private int enchantability;

    private EnumCapeMaterial(int damageReductionAmount, int enchantability) {
        this.damageReductionAmount = damageReductionAmount;
        this.enchantability = enchantability;
    }
    
    public int getDamageReductionAmount() {
        return this.damageReductionAmount;
    }
    
    public int getEnchantability() {
        return this.enchantability;
    }
}
