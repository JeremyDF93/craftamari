package net.minecraft.src;

import craftamari.item.ItemCape;

public class SlotCape extends Slot {
    public SlotCape(IInventory iInventory, int id, int x, int y) {
        super(iInventory, id, x, y);
    }
    
    public boolean isItemValid(ItemStack itemStack) {
        return itemStack.getItem() instanceof ItemCape;
    }
    
    public Icon getBackgroundIconIndex() {
        return ItemCape.getIconBackground();
    }
}
