package net.minecraft.src;

import net.minecraft.src.ItemStack;

public class RecipesGeneral {

    public void addRecipes(CraftingManager craftingManager) {
    	craftingManager.addRecipe(new ItemStack(Item.obsidianRing, 4, 0), new Object[] {" # ", "# #", " # ", '#', Block.obsidian});
    	craftingManager.addRecipe(new ItemStack(Item.refinedRing, 1, 0), new Object[] {" # ", "#X#", " # ", '#', Item.blazePowder, 'X', Item.obsidianRing});
    	craftingManager.addShapelessRecipe(new ItemStack(Item.obsidianSheet), Item.obsidianRing, Item.obsidianRing, Item.obsidianRing, Item.obsidianRing);
    	craftingManager.addShapelessRecipe(new ItemStack(Item.refinedSheet), Item.refinedRing, Item.refinedRing, Item.refinedRing, Item.refinedRing);
    }
}