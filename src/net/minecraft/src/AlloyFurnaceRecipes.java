package net.minecraft.src;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import net.minecraft.src.ItemStack;

public class AlloyFurnaceRecipes {
    private static HashMap<ItemStack, ArrayList<ItemStack>> smeltingList = new HashMap<ItemStack, ArrayList<ItemStack>>();
    private static HashMap<ItemStack, Float> experienceList = new HashMap<ItemStack, Float>();
    
    static {
        addSmelting(new ItemStack(Item.ingotBronze), 0.7F, new Object[] { Block.oreCopper, Block.oreTin });
        addSmelting(new ItemStack(Item.ingotSteel), 0.7F, new Object[] { Block.oreIron, new ItemStack(Item.coal, 1) });
        addSmelting(new ItemStack(Item.ingotMithril), 1.0F, new Object[] { Block.oreMithril, new ItemStack(Item.coal, 2) });
        addSmelting(new ItemStack(Item.ingotAdamant), 1.3F, new Object[] { Block.oreAdamant, new ItemStack(Item.coal, 4) });
        addSmelting(new ItemStack(Item.ingotRune), 1.6F, new Object[] { Block.oreRune, new ItemStack(Item.coal, 6) });
    }

    private static void addSmelting(ItemStack itemStack1, float experience, Object[] objects) {
        ArrayList<ItemStack> arrayList = new ArrayList<ItemStack>();
        
        for (Object object : objects) {
            ItemStack itemStack2;
            
            if (object instanceof Item) {
                itemStack2 = new ItemStack((Item) object);
            } else if (object instanceof ItemStack) {
                itemStack2 = ((ItemStack) object).copy();
            } else {
                if (!(object instanceof Block)) {
                    throw new RuntimeException("Invalid smelting recipe!");
                }
                
                itemStack2 = new ItemStack((Block) object);
            }
            
            arrayList.add(itemStack2);
        }
        
        smeltingList.put(itemStack1, arrayList);
        experienceList.put(itemStack1, experience);
    }
    
    public static boolean canSmelt(ItemStack itemStack1) {
        if (itemStack1 == null) {
            return false;
        }
        
        for (Entry<ItemStack, ArrayList<ItemStack>> entry : smeltingList.entrySet()) {
            ArrayList<ItemStack> arrayList = entry.getValue();
            
            for (ItemStack itemStack2 : arrayList) {
                if (itemStack1.itemID == itemStack2.itemID && itemStack1.getItemDamage() == itemStack2.getItemDamage()) {
                    return true;
                }
            }
        }
        
        return false;
    }
    
    public static int getRequiredAmountBySlot(ItemStack[] itemStacks, int slot) {
        for (Entry<ItemStack, ArrayList<ItemStack>> entry : smeltingList.entrySet()) {
            ArrayList<ItemStack> arrayList = entry.getValue();
            
            if (isMatching(arrayList, itemStacks)) {
                for (ItemStack itemStack : arrayList) {
                    if (itemStacks[slot].itemID == itemStack.itemID && itemStacks[slot].getItemDamage() == itemStack.getItemDamage()) {
                        return itemStack.stackSize;
                    }
                }
            }
        }
        
        return 0;
    }
    
    public static ItemStack getSmeltingResult(ItemStack[] itemStacks) {
        for (Entry<ItemStack, ArrayList<ItemStack>> entry : smeltingList.entrySet()) {
            ArrayList<ItemStack> arrayList = entry.getValue();
            
            if (isMatching(arrayList, itemStacks)) {
                return entry.getKey();
            }
        }
        
        return null;
    }
    
    private static boolean isMatching(ArrayList<ItemStack> arrayList1, ItemStack[] itemStacks) {
        ArrayList<ItemStack> arrayList2 = new ArrayList<ItemStack>(arrayList1);
        
        for (ItemStack itemStack1 : itemStacks) {
            if (itemStack1 != null) {
                boolean flag = false;
                
                for (ItemStack itemStack2 : arrayList2) {
                    if (itemStack1.itemID == itemStack2.itemID && itemStack1.getItemDamage() == itemStack2.getItemDamage() && itemStack1.stackSize >= itemStack2.stackSize) {
                        flag = true;
                        arrayList2.remove(itemStack2);
                        break;
                    }
                }
                
                if (!flag) {
                    return false;
                }
            }
        }
        
        return arrayList2.isEmpty();
    }

    public static float getExperience(ItemStack itemStack1) {
        if (itemStack1 == null) {
            return 0;
        }
        
        for (Entry<ItemStack, Float> entry : experienceList.entrySet()) {
            ItemStack itemStack2 = entry.getKey();
            
            if (itemStack1.itemID == itemStack2.itemID && itemStack1.getItemDamage() == itemStack2.getItemDamage()) {
                return entry.getValue();
            }
        }
        
        return 0;
    }
}
